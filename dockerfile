# we are extending everything from tomcat:8.0 image ...
FROM tomcat:9.0


# COPY path-to-your-application-war path-to-webapps-in-docker-tomcat
COPY  ./my-app-1.0-SNAPSHOT.jar /usr/local/tomcat/webapps/
