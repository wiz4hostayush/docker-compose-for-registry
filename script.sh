#! /bin/bash
docker-compose up -d
docker update --restart unless-stopped $(docker ps -q)
docker run -d --restart unless-stopped registry